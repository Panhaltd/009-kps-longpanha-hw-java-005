public class PrintString extends Thread{
    String msg;
    int speed;
    PrintString(String msg,int speed){
        this.msg=msg;
        this.speed=speed;
    }
    public void run(){
        printMsg(msg,speed);
    }
    synchronized void printMsg(String str,int s){
        for (int i=0;i<str.length();i++){
            try {
                sleep(s);
                System.out.print(str.charAt(i));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public static void main(String[] args) {
        PrintString t1 = new PrintString("Hello KSHRD!\n",150);
        PrintString t2 = new PrintString("*************************************\n" +
                                              "I will try my best to be here at HRD.\n" +
                                              "-------------------------------------\n",150);
        PrintString t3 = new PrintString("Downloading",30);
        PrintString t4 = new PrintString("............",300);
        PrintString t5 = new PrintString("Completed 100%\n",30);
        try {
            t1.start();
            t1.join();
            t2.start();
            t2.join();
            t3.start();
            t3.join();
            t4.start();
            t4.join();
            t5.start();
            t5.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
